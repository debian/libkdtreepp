libkdtree++ (0.7.3-1) unstable; urgency=medium

  * QA upload.

  * Corrected Vcs links in d/control.
  * Added build dependency on python3-setuptools (Closes: #1080640).
  * Corrected upstream source URL in d/copyright.
  * New upstream version 0.7.3
    - Dropped gcc-5.patch now included upstream.
    - Refreshed enable-tests.patch and d/docs.

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 23 Nov 2024 17:29:25 +0100

libkdtree++ (0.7.1+git20101123-6) unstable; urgency=medium

  * QA upload.
  * debian/control:
    + Bump Standards-Version to 4.6.0.
    + Bump debhelper compat to v13.
    + Add Vcs-* fields.
    + Update homepage to use GitHub project.
    + Mark package libkdtree++-dev as Multi-Arch: foreign.
  * debian/watch: Fix upstream monitoring and use v4 format.
  * debian/rules: Refresh packaging instructions.
    + Drop manual -dbg packages. (Closes: #994305)

 -- Boyuan Yang <byang@debian.org>  Sun, 21 Nov 2021 13:27:14 -0500

libkdtree++ (0.7.1+git20101123-5) unstable; urgency=medium

  * QA upload.
  * Stop building the Python 2 packages. Closes: #936884.
  * Fix installation of Python 3 extensions. Closes: #942672.
  * Bump standards version.

 -- Matthias Klose <doko@debian.org>  Sat, 26 Oct 2019 18:03:05 +0200

libkdtree++ (0.7.1+git20101123-4) unstable; urgency=medium

  * debian/patches/gcc-5.patch: Fix build with gcc 5. (Closes: #777951)
  * debian/control:
    - Add dh-python to B-D.
    - Bump Standards-Version to 3.9.6.

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 02 Mar 2015 23:43:04 +0100

libkdtree++ (0.7.1+git20101123-3) unstable; urgency=medium

  * debian/python3-kdtree{-dbg}.install: Fix wildcard so that dh_install does
    not fail. (Closes: #735820)
  * debian/rules: Make test targets not fail if nocheck is given in
    DEB_BUILD_OPTIONS.
  * debian/control: Bump Standards-Version (no changes required).

 -- Sebastian Ramacher <sramacher@debian.org>  Fri, 17 Jan 2014 22:10:49 +0100

libkdtree++ (0.7.1+git20101123-2) unstable; urgency=low

  * Upload to unstable.
  * debian/copyright: Convert to Copyright Format 1.0.
  * debian/rules: Replace override_dh_install with proper install files.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 05 May 2013 02:43:45 +0200

libkdtree++ (0.7.1+git20101123-1) experimental; urgency=low

  * New git snapshot. (Closes: #559306)
    - Add get-orig-source rule to debian/rules to create the snapshot tarball.
  * Convert to 3.0 (quilt).
  * debian/control:
    - Update my mail address.
    - Add Homepage field.
    - Add ${misc:Depends} to Depends.
    - Bump debhelper Build-Dep to >= 9.
  * debian/compat: Bump to 9.
  * debian/watch: Add watch file.
  * debian/copyright:
    - Update my mail address.
    - Add Willi Richert.
    - Update Sylvain Bougerel's and Paul Harris' copyright years.
  * Ship examples in libkdtree++-dev. (Closes: #527370)
  * Ship Python bindings. (Closes: #559307)
    - debian/control: Add python-kdtree{,-dbg} and python3-kdtreee{,-dbg}
      packages.
    - debian/rules: Rewrite to build Python bindings for all supported Python
      versions.
    - debian/control: Add Build-Dep on swig, cmake, python-all{,-dbg} and
      python3-all{,-dbg}.
    - debian/patches: Add a series of patches to build and install the Python
      bindings properly.
    - debian/clean: Clean pyc files generated while running the tests.
  * debian/README.Debian: Removed, not needed anymore.

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 31 Dec 2012 00:25:37 +0100

libkdtree++ (0.7.0-2) unstable; urgency=low

  * New maintainer. (Closes: #689206)
    Thanks to Martin Schreiber for maintaining libkdtree++!
  * debian/copyright:
    - List the copyright of the authors. (Closes: #688796)
    - Mention the download location.
    - List the Debian maintainers.
    - Update URL of the Artistic License.
  * debian/README.Debian: Document that this is a snapshot of 222b5d77 and not
    0.7.0.

 -- Sebastian Ramacher <s.ramacher@gmx.at>  Thu, 11 Oct 2012 21:24:38 +0200

libkdtree++ (0.7.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Cherry pick 8d4fbb9a from upstream to fix issues with g++ 4.7. (Closes:
    #687604)

 -- Sebastian Ramacher <s.ramacher@gmx.at>  Tue, 25 Sep 2012 16:57:06 +0200

libkdtree++ (0.7.0-1) unstable; urgency=low

  * New upstream release (closes: Bug#506485)

 -- Martin Schreiber <schreiberx@gmail.com>  Tue, 30 Dec 2008 10:34:06 +0100

libkdtree++ (0.6.2-1) unstable; urgency=low

  * New upstream version (closes: Bug#459106)
  * New maintainer

 -- Martin Schreiber <schreiberx@gmail.com>  Fri, 08 Feb 2008 22:52:21 +0100

libkdtree++ (0.2.0-1) unstable; urgency=low

  * New upstream version, thanks to Paul Harris.
  * Closes bugs from 0.1.3-1, which was not officially uploaded to Debian
    (closes: Bug#279614, Bug#279620).

 -- martin f. krafft <madduck@debian.org>  Mon, 15 Nov 2004 09:40:36 +0100

libkdtree++ (0.1.3-1) unstable; urgency=low

  * New upstream version (closes: Bug#279614, Bug#279620).
  * Pushed Standards-Version to 3.6.1.1

 -- martin f. krafft <madduck@debian.org>  Thu,  4 Nov 2004 10:29:23 +0100

libkdtree++ (0.1.2-1) unstable; urgency=low

  * Added note about lack of runtime and shared libraries.
  * Changed licence to the artistic licence.

 -- martin f. krafft <madduck@debian.org>  Sun, 16 May 2004 00:52:10 +0200

libkdtree++ (0.1.1-1) unstable; urgency=low

  * New upstream release.

 -- martin f. krafft <madduck@debian.org>  Tue, 11 May 2004 19:48:32 +0200

libkdtree++ (0.1.0-1) unstable; urgency=low

  * Initial Release. (closes: Bug#248219)

 -- martin f. krafft <madduck@debian.org>  Tue, 11 May 2004 15:49:02 +0200

